<?php

// include composer packages
include "vendor/autoload.php";
define('FPDF_FONTPATH','pdf-thai/fpdf17/font/');



$data = json_decode(file_get_contents('php://input'));;

return createPDF($data);


function createPDF($data){

    $pdf = new \setasign\Fpdi\Fpdi();
    // Reference the PDF you want to use (use relative path)
    $pagecount = $pdf->setSourceFile( 'central.pdf' );
    // Import the first page from the PDF and add to dynamic PDF

    $pages = $data->pages;
    $tpl = $pdf->importPage(1);
    $size = $pdf->getTemplateSize($tpl);

    foreach ($pages as $page){
        $pdf = generateInvoicePage($pdf,$page,$tpl,$size);
    }


// render PDF to browser
    $pdf->Output();
}

function generateInvoicePage(\setasign\Fpdi\Fpdi $pdf,$data,$tpl,$size){
    $pdf -> AddPage();
    $pdf ->useTemplate($tpl, null, null, $size['width'],  $size['height'], true);

    // Set the default font to use
    $pdf->AddFont('angsa','','angsa.php');
    $pdf->SetFont('angsa');

    // First box - the user's Name
    $pdf = setName($pdf,$data);

    $pdf = setAddress($pdf,$data);
    $pdf = setPayment($pdf,$data);
    $pdf = setItemsQuantity($pdf,$data);
    $pdf = setPOSDate($pdf,$data);
    $pdf = setOrderID($pdf,$data);
    $pdf = setDeliveryID($pdf,$data);
//    $pdf = ImprovedTable($pdf,$data);

    return $pdf;
}

function setName(\setasign\Fpdi\Fpdi $pdf,$data){

    $pdf->SetFontSize('14'); // set font size

    $pdf->SetXY(24,20);

    // $pdf->Cell( $width, $height, $text, $border, $fill, $align);
    $pdf->Cell(30, 10, iconv( 'UTF-8','TIS-620',$data->name), 0, 0, 'L');

    return $pdf;
}

function setAddress(\setasign\Fpdi\Fpdi $pdf,$data){


    $pdf->SetFontSize('14'); // set font size

    $pdf->SetXY(24,37);

    // $pdf->Cell( $width, $height, $text, $border, $fill, $align);
    $pdf->MultiCell(85, 4, iconv( 'UTF-8','TIS-620',$data->address));

    return $pdf;
}


function setPayment(\setasign\Fpdi\Fpdi $pdf, $data){


    $pdf->SetFontSize('12'); // set font size

    $pdf->SetXY(30,58);
    // $pdf->Cell( $width, $height, $text, $border, $fill, $align);
    $pdf->Cell(30, 10, iconv( 'UTF-8','TIS-620',$data->payment_type), 0, 0, 'L');

    $pdf->SetXY(95,58);
    // $pdf->Cell( $width, $height, $text, $border, $fill, $align);
    $pdf->Cell(30, 10, iconv( 'UTF-8','TIS-620',$data->amount), 0, 0, 'L');

    return $pdf;

}

function setItemsQuantity(\setasign\Fpdi\Fpdi $pdf, $data){


    $pdf->SetFontSize('12'); // set font size

    $pdf->SetXY(33,68);
    $pdf->Cell(10, 10, iconv( 'UTF-8','TIS-620',$data->boxes), 0, 0, 'R');

    $pdf->SetXY(64,68);
    $pdf->Cell(10, 10, iconv( 'UTF-8','TIS-620',$data->bags), 0, 0, 'R');

    $pdf->SetXY(89,68);
    $pdf->Cell(10, 10, iconv( 'UTF-8','TIS-620',$data->pieces), 0, 0, 'R');

    $pdf->SetXY(33,80);
    $pdf->Cell(10, 10, iconv( 'UTF-8','TIS-620',$data->special_boxs), 0, 0, 'R');

    $pdf->SetXY(64,80);
    $pdf->Cell(10, 10, iconv( 'UTF-8','TIS-620',$data->special_bags), 0, 0, 'R');

    return $pdf;
}


function setPOSDate(\setasign\Fpdi\Fpdi $pdf, $data){


    $pdf->SetFontSize('12'); // set font size

    $pdf->SetXY(24,90);
    $pdf->Cell(10, 10, iconv( 'UTF-8','TIS-620',$data->pos_date), 0, 0, 'L');

    return $pdf;
}

function setOrderID(\setasign\Fpdi\Fpdi $pdf, $data){

    $pdf->SetFontSize('32'); // set font size

    $pdf->SetXY(127,34);
    $pdf->Cell(10, 10, iconv( 'UTF-8','TIS-620',$data->order_id), 0, 0, 'L');



    return $pdf;
}

function setDeliveryID(\setasign\Fpdi\Fpdi $pdf, $data){

    $generatorJPG = new Picqer\Barcode\BarcodeGeneratorJPG();
    $image = $generatorJPG->getBarcode($data->delivery_id, $generatorJPG::TYPE_CODE_128);


    $output = "images/barcode/".$data->delivery_id.".jpg";
    file_put_contents($output, $image);

    //Image($file, $x=null, $y=null, $w=0, $h=0, $type='', $link='')
    $pdf-> Image($output,153,59,61,5);

    $pdf->SetFontSize('20'); // set font size

    $pdf->SetXY(153,61);

    $delivery_id = "";
    $delivery_id_array = str_split($data->delivery_id);
    foreach ($delivery_id_array as $char){
        $delivery_id = $delivery_id." ".$char ;
    }

    $pdf->Cell(61, 10, iconv( 'UTF-8','TIS-620',"*".$delivery_id." *"), 0, 0, 'C');

    return $pdf;
}

// Better table
function ImprovedTable(\setasign\Fpdi\Fpdi  $pdf, $table_data)
{

    $pdf->SetXY(7,179);
    $pdf->SetLeftMargin(7);

    $header =
        [ "ลำดับ","PID","ProductShortName", "จำนวน","หน่วย" ];


    // Column widths
    $pdf->SetFillColor(189,189,189);
    $pdf->SetDrawColor(128,0,0);
    $w = array(22, 22, 132, 28,28);
    // Header
    for($i=0;$i<count($header);$i++)
        $pdf->Cell($w[$i],12, iconv( 'UTF-8','TIS-620',$header[$i]) ,1,0,'C',true);
    $pdf->Ln();
    // Data
    foreach($table_data as $row)
    {

        $pdf->Cell($w[0],8,iconv( 'UTF-8','TIS-620',$row[0]),'LRB',0, "R");
        $pdf->Cell($w[1],8,iconv( 'UTF-8','TIS-620',$row[1]),'LRB',0, "C");
        $pdf->Cell($w[2],8,iconv( 'UTF-8','TIS-620',$row[2]),'LRB',0,'L');
        $pdf->Cell($w[3],8,iconv( 'UTF-8','TIS-620',$row[3]),'LRB',0,'C');
        $pdf->Cell($w[4],8,iconv( 'UTF-8','TIS-620',$row[4]),'LRB',0,'C');
        $pdf->Ln();
    }
    // Closing line
    $pdf->Cell(array_sum($w),0,'','T');

    return  $pdf;
}


